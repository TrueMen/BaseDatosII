# **BASE DE DATOS II**

## **Unidad I**

### **Actividad 1.1 .- Elaborar mapas conceptuales sobre el modelo relacional**
## Autores: Alexis german Hurtado
## Profesor: Ivan Chenowet
# INTRODUCCION
En esta presentacion se dara a conocer sobre  el modelo relacional
con finalidad de comprender el tema explicando atravez de un mapa
conceptual la definicion,Diferencias,ventajas y desventajas
El modelo relacional es un modelo de datos y, como tal, tiene en cuenta los tres aspectos siguientes de los datos:
1) La estructura, que debe permitir representar la información que nos interesa del mundo real.
2) La manipulación, a la que da apoyo mediante las operaciones de actualización y consulta de los datos
3) La integridad, que es facilitada mediante el establecimiento de reglas de integridad; es decir, condiciones que los datos deben cumplir.
# MAPA CONCEPTUAL 
(https://drive.google.com/file/d/1XA_rQ-yk-_1SAJs3kX4lB0UudOZnC_Dc/view?usp=sharing)

# CONCLUSION
El modelo relacional propone una representación de la información que origine esquemas 
que representen fielmente la información los objetos y las relaciones, y que además sea
fácilmente entendida por usuarios, siendo posible ampliar el esquema de la BD sin modificar
la estructura lógica. 
Además debe permitir flexibilidad en la formulación de los interrogantes sobre los datos.
## Biblografía
(http://ict.udlap.mx/people/carlos/is341/bases02.html)
(https://es.slideshare.net/vyezk007/integridad-de-entidad-e-integridad-referencial-en-sql-server-y-access)

