# **BASE DE DATOS II**

## **Unidad I**

### **Actividad 1.2 .- Caso de estudio de transformación de un Modelo relacional hacia un diagrama de Entidad Relación **
## Autores: Alexis german Hurtado
## Profesor: Ivan Chenowet
# INTRODUCCION
En esta presentacion se dara a conocer sobre  el entidad relacion
con finalidad de comprender el modelo relacion y entidad
el tema sobre como funciona un menu en un restauran o en algun lugar de comida
con estos diagramas pueden comprender los procesos que conlleva
un menu.
# Diagrama de Menu
(https://drive.google.com/drive/u/0/my-drive)
# Entidad Relacion
(https://drive.google.com/drive/u/0/my-drive)

# CONCLUSION
Éste es el modelo conceptual más utilizado para el diseño conceptual de bases de datos.
Ahora se utiliza el denominado modelo entidad-relación extendido.
En los que aparte de entidad, relación y atributos, también hay atributos compuestos y jerarquías de generalización.

•Entidad (se representa con un rectángulo) dos tipos de entidades: fuertes y débiles. Una entidad débil es una entidad cuya existencia depende de la existencia de otra entidad.
•Relación recursiva es una relación donde la misma entidad participa más de una vez en la relación con distintos papeles.


